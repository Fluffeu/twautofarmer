var scavTemplates = [
    {
        "name": "timepad",
        "initialWaitTime": 100,
        "waitBetweenLevelsScale": 3.0,
        "strategy": "padToMaxTime", // padToMaxTime, equalDistribution
        "maxTime": "2:30:00",
        "keepUnits": [0, 0, 0, 0, 99999, 99999, 99999, 99999]
    },
    {
        "name": "max",
        "initialWaitTime": 100,
        "waitBetweenLevelsScale": 3.0,
        "strategy": "equalDistribution", // padToMaxTime, equalDistribution
        "maxTime": "12:00:00",
        "keepUnits": [0, 0, 0, 0, 0, 0, 0, 99999]
    },
]

const SCAVENGE_UNITS = ["spear", "sword", "axe", "archer", "light", "marcher", "heavy", "knight"]
const UNITS_CAPACITY = [25, 15, 10, 10, 80, 50, 50, 100]
const LEVELS_EFFICIENCY = [ 0.1, 0.25, 0.5, 0.75 ]

var squadsSent = 0


/* Add Autofarm button to bottom toolbar if we're on farm assistant's page */
if (/mode=scavenge/.test(location.href)) {
    addScavActivationButtons()
}


function addScavActivationButtons() {
    for (let i = 0; i < scavTemplates.length; i++) {
        let name = scavTemplates[i]["name"]
        let template = scavTemplates[i]

        const newButton = document.createElement("a")
        newButton.innerText = `Scavenge-${name}`
        newButton.className = "footer-link"
        newButton.id = `autoscavenger-${name}`
        const separator = document.createTextNode(` - `)

        newButton.onclick = function() {
            console.log("running with template: " + JSON.stringify(template))
            runAutoscavenger(template)
        }

        document.getElementById("linkContainer").appendChild(separator)
        document.getElementById("linkContainer").appendChild(newButton)
    }
}


function runAutoscavenger(settings) {
    console.log("Running autoscavenger")
    waitForNextAction(1, settings["initialWaitTime"], settings["initialWaitTime"]).then(()=>{

    let units = getScavengeUnits(settings)
    console.log("units: " + JSON.stringify(units))
    let levels = getScavengeLevelsData()
    console.log("levels: " + JSON.stringify(levels))
    let sendData = calculateSendData(units, levels, settings)
    console.log("send data: " + JSON.stringify(sendData))

    sendSquad(levels, sendData, 0, settings)

    })
}


function scavPostRun() {
    if (squadsSent > 0) {
        console.log("sending tooldone event")
        document.dispatchEvent(new Event("tool done"))
    }
    else {
        console.log("sending nothingtodo event")
        document.dispatchEvent(new Event("nothing to do"))
    }
}


async function sendSquad(levels, sendData, sendI, settings) {
    if (sendI >= sendData.length) {
        scavPostRun()
        return
    }
    console.log("sending squad " + sendI)
    squadsSent += 1

    fillSquadData(sendData[sendI]["units"])
    waitForNextAction(1.2).then(()=>{
    clickButton(levels[sendData[sendI]["level"]]["sendButton"])
    waitForNextAction(settings["waitBetweenLevelsScale"]).then(()=>{
    sendSquad(levels, sendData, sendI+1, settings)

    })})
}


function fillSquadData(units) {
    for (let i = 0; i < SCAVENGE_UNITS.length; i++) {
        if (units[i] != 0) {
            enterInputValue(getUnitElement(i).firstElementChild, units[i])
        }
    }
}


function calculateSendData(units, levels, settings) {
    if (settings["strategy"] === "equalDistribution") {
        return strategyEqualDistribution(units, levels, settings)
    }
    else if (settings["strategy"] === "padToMaxTime") {
        return strategyPadToMaxTime(units, levels, settings)
    }
}


function strategyEqualDistribution(units, levels, settings) {
    let ret = []
    let maxTime = timeToSeconds(settings["maxTime"])

    for (let i = 0; i < levels.length; i++) {
        let calculatedUnits = []
        let send = false

        for (let i = 0; i < units.length; i++) {
            let n = parseInt(units[i] / (levels.length - ret.length))
            calculatedUnits.push(n)
            units[i] -= n
            send = send || n > 0
        }

        let maxLoot = timeToLoot(maxTime, levels[i]["levelId"])
        let currentLoot = getUnitsCapacity(calculatedUnits)
        console.log("maxLoot: " + maxLoot)
        console.log("currentLoot: " + currentLoot)
        if (currentLoot > maxLoot) {
            let ratio = maxLoot/currentLoot

            for (let i = 0; i < units.length; i++) {
                calculatedUnits[i] *= ratio
            }
        }

        if (send) {
            ret.push({
                "units": calculatedUnits,
                "level": i
            })
        }
    }

    return ret
}


function strategyPadToMaxTime(units, levels, settings) {
    let ret = []
    let availableCapacity = getUnitsCapacity(units)

    for (let i = levels.length - 1; i >= 0; i--) {
        let calculatedUnits = []
        let send = false
        let loot = timeToLoot(timeToSeconds(settings["maxTime"]), levels[i]["levelId"])
        
        if (availableCapacity > loot) {
            let ratio = loot/availableCapacity
            console.log("ratio: " + ratio)
            for (let u = 0; u < units.length; u++) {
                console.log("units[u]: " + units[u])
                calculatedUnits.push(parseInt(units[u]*ratio))
                units[u] -= calculatedUnits[u]
                send = send || calculatedUnits[u] > 0
            }
        }
        else {
            calculatedUnits = units
            send = getUnitsCapacity(units) > 0
        }

        if (send) {
            ret.push({
                "units": calculatedUnits,
                "level": levels[i]["levelId"]
            })

            availableCapacity -= getUnitsCapacity(calculatedUnits)

            if (availableCapacity <= 0) {
                break
            }
        }
    }

    return ret
}


function getScavengeLevelsData() {
    let ret = []

    for (let i = 0; i < 4; i++) {
        let actionsContainers = document.getElementsByClassName("options-container")[0].childNodes[i].getElementsByClassName("action-container")
        if (actionsContainers.length > 0 && actionsContainers[0].childNodes.length > 1) {
            ret.push({
                "levelId": i,
                "sendButton": actionsContainers[0].firstElementChild
            })
        }
    }

    return ret
}


function getScavengeUnits(settings) {
    let ret = []

    for (let i = 0; i < SCAVENGE_UNITS.length; i++) {
        let elem = getUnitElement(i)
        let unitCount = elem.getElementsByTagName("a")[0].innerHTML
        unitCount = parseInt(unitCount.substring(1, unitCount.length - 1))
        unitCount = Math.max(0, unitCount - settings["keepUnits"][i])
        ret.push(unitCount)
    }

    return ret
}


function getUnitElement(unitId) {
    return document.getElementsByClassName("candidate-squad-widget")[0].firstElementChild.getElementsByTagName("tr")[1].getElementsByTagName("td")[unitId]
}


function getUnitsCapacity(units) {
    let capacity = 0
    for (let i = 0; i < UNITS_CAPACITY.length; i++) {
        capacity += UNITS_CAPACITY[i]*units[i]
    }

    return capacity
}


function lootToTime(loot, levelId) {
    let income = loot * LEVELS_EFFICIENCY[levelId]
    return income**0.9 * 7.02586001978 + 1592.1
}


function timeToLoot(time, levelId) {
    return (((time - 1592.1)/7.02586001978) ** 1.11111111)/LEVELS_EFFICIENCY[levelId]
}