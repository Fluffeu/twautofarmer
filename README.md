## About

TWAutofarmer is a browser addon for Tribal Wars game, that automatically sends farming attacks.

## Prerequisites

- internet browser that accepts unsigned addons (Opera or Librewolf seemed to work for me),
- Farm Assistant premium feature in Tribal Wars.