function clickButton(element) {
    element.dispatchEvent(new Event("focus"))
    // waitForNextAction(1, 10, 50).then(()=>{
    element.click()

    // })
}


function enterInputValue(element, value) {
    element.dispatchEvent(new Event("focus"))
    // waitForNextAction(1, 12, 15).then(()=>{
    element.dispatchEvent(new Event("keydown"))
    // waitForNextAction(1, 12, 15).then(()=>{
    element.value = value
    // waitForNextAction(1, 50, 70).then(()=>{
    element.dispatchEvent(new Event("keyup"))
    element.dispatchEvent(new Event("change"))

    // })})})
}


function waitForNextAction(delayScale = 1.0, min = 210, max = 800) {
    let rounds = 10
    let drift = 0.0
    for (let i = 0; i < rounds; i++) {
        drift += Math.random() 
    }
    drift = Math.abs(drift - rounds/2.0)
    drift /= rounds/2
    let delay = min + drift*(max - min)
    // console.log("delay: " + delay*delayScale)
    return new Promise(r => setTimeout(r, delay*delayScale))
}


function dateTillNowToSeconds(dateStr) {
    let currentTime = document.getElementById("serverTime").innerText
    let parsed = /(.*) o (\d{2}:\d{2}:\d{2})/.exec(dateStr)
    let daysAgo = 0

    if (parsed[1] === "dzisiaj") {
        daysAgo = 0
    }
    else if (parsed[1] === "wczoraj") {
        daysAgo = 1
    }
    else {
        daysAgo = 2 // no point in parsing dates older than 3 days for farming purposes
    }

    let res = daysAgo * 24 * 60 * 60
    res += timeToSeconds(currentTime)
    res -= timeToSeconds(parsed[2])

    return res
}


function timeToSeconds(timeStr) {
    let s = timeStr.split(":")
    let seconds = parseInt(s[2])
    seconds += 60 * parseInt(s[1])
    seconds += 3600 * parseInt(s[0])

    return seconds
}


function isInt (value) {
    return /^\d+$/.test(value);
}