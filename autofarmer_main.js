var farmTemplates = [
    {
        "name": "main",
        "id": 0,
        "reloadWaitScale": 2.0,
        "minUnits": 3,
        "rules":
        [
            {
                "condition": { "wall": "<2", "distance": "<50", "isAttacked": "0", "cAvailable": "1" },
                "units":
                { 
                    "strategy": {"type": "interpolate", "key": "distance", "order": "linear", "reference": "cGroup", "values": {0.0: "100%", 10.0: "100%", 50.0: "0%"}},
                    "min": 2,
                    "max": 40 
                }
            }
        ],
    },
    {
        "name": "test",
        "id": 1,
        "reloadWaitScale": 2.0,
        "minUnits": 3,
        "rules":
        [
            {
                "condition": { "wall": "<2", "distance": "<50", "isAttacked": "0", "cAvailable": "0", "secondsAgo": ">86400" },
                "units":
                { 
                    "strategy": {"type": "constant", "value": 4}
                }
            },
            {
                "condition": { "wall": "<2", "distance": "<50", "isAttacked": "0", "cAvailable": "1" },
                "units":
                { 
                    "strategy": {"type": "interpolate", "key": "secondsAgo", "order": "linear", "reference": "income", "values": {0.0: "10000%", 7200.0: "15000%"}},
                    "min": 2,
                    "max": 50 
                }
            }
        ],
    },
]


var villagesData = []

var currentATemplate = {}
var currentBTemplate = {}
var nextTemplates = []
var autostarted = false
var attacksSent = 0

var unitsNotSent = 0 /* when autofarming, keep track of the number of units not sent, that will need to be send after template updates */

const TEMPLATE_UNITS = ["spear", "sword", "axe", "archer", "spy", "light", "marcher", "heavy", "knight"]


/* Add Autofarm button to bottom toolbar if we're on farm assistant's page */
if (/screen=am_farm/.test(location.href)) {
    console.log("halo xd")
    addFarmActivationButtons()
    chrome.storage.local.get([ "sendImmediately", "template" ], function(result) {
        if (result.sendImmediately && result.template !== undefined && result.template >= 0) {
            autostarted = true
            waitForNextAction(farmTemplates[result.template]["reloadWaitScale"]).then( () => {
                runAutofarmer(farmTemplates[result.template])
            })
        }
    })
}


function addFarmActivationButtons() {
    for (let i = 0; i < farmTemplates.length; i++) {
        let name = farmTemplates[i]["name"]
        let template = farmTemplates[i]

        const newButton = document.createElement("a")
        newButton.innerText = `Farm-${name}`
        newButton.className = "footer-link"
        newButton.id = `autofarmer-${name}`
        const separator = document.createTextNode(` - `)

        newButton.onclick = function() {
            console.log("running with template: " + JSON.stringify(template))
            runAutofarmer(template)
        }

        document.getElementById("linkContainer").appendChild(separator)
        document.getElementById("linkContainer").appendChild(newButton)
    }
}


function runAutofarmer(settings) {
    chrome.storage.local.set({sendImmediately: false, template: -1})
    console.log("Running autofarmer")
    loadVillagesData()
    loadCurrentTemplates()
    handleAttack(0, settings)
}


function farmPostRun(settings) {
    console.log("farmPostRun")
    // console.log("next templates: " + JSON.stringify(nextTemplate))
    if (nextTemplates.length > 0 && villagesData.length > 0 && getUnitsSum(getAvailableUnits()) >= settings["minUnits"]) {
        waitForNextAction(settings["reloadWaitScale"]).then( () => {
            chrome.storage.local.set({sendImmediately: true, template: settings["id"]})
            changeTemplates()
        })
    }
    else if (autostarted || (unitsNotSent >= settings["minUnits"] && nextTemplates.length > 0)) {
        console.log("sending tooldone event")
        document.dispatchEvent(new Event("tool done"))
    }
    else {
        console.log("sending nothingtodo event")
        document.dispatchEvent(new Event("nothing to do"))
    }
}


function handleAttack(villageI, settings) {
    let availableUnits = getAvailableUnits()
    let availableSum = getUnitsSum(availableUnits)
    if (unitsNotSent >= availableSum || availableSum < settings["minUnits"] || villageI > villagesData.length || villagesData[villageI] === undefined) {
        console.log("units not sent: " + unitsNotSent)
        console.log("available units: " + JSON.stringify(availableUnits))
        farmPostRun(settings)
        return
    }

    let village = villagesData[villageI]
    let wait = false
    let ruleId = checkFarmRules(village, settings["rules"])

    if(ruleId >= 0)
    {
        wait = handleCustomAttack(village, availableUnits, settings["rules"][ruleId]["units"])
    }
    else {
        console.log("village filtered out")
    }

    if (wait) {
        waitForNextAction()
            .then( () => {
                handleAttack(villageI + 1, settings)
            })
    }
    else {
        handleAttack(villageI + 1, settings)
    }
}


function checkFarmRules(villageData, rules) {
    for (let i = 0; i < rules.length; i++) {
        let passed = true
        let condition = rules[i]["condition"]

        Object.keys(condition).forEach(key => {
            if (passed) {
                let param = villageData[key]
                let value = condition[key]
                let action = "="
                
                if (value[0] === "<" || value[0] === ">") {
                    action = value[0]
                    value = parseInt(value.substring(1, value.length))
                }
                else {
                    value = parseInt(value)
                }

                if (param === undefined) {
                    passed = false
                }
                else if (action === "=" && param != value) {
                    passed = false
                }
                else if (action === ">" && param <= value) {
                    passed = false
                }
                else if (action === "<" && param >= value) {
                    passed = false
                }
            }
        })

        if (passed) {
            return i
        }
    }

    return -1
    // return (settings["notAttacked"] && villageData["isAttacked"]) || villageData["wall"] > settings["maxWall"] || villageData["distance"] > settings["maxDist"]
}


function getCAttackUnits(villageData, maxUnits, unitsDesc) {
    let unitsDistr = {}
    // let villageIncome = villageData["resources"]/(villageData["secondsAgo"] + villageData["distance"]*600)
    // console.log("villageIncome: " + villageIncome)
    // let f = Math.max(0.1, Math.min(1.0, 1.25 - villageData["distance"]/40))

    Object.keys(villageData["recommendedUnits"]).forEach(key => {
        if (key === "spy") {
            unitsDistr[key] = 1
        }
        else {
            unitsDistr[key] = calculateRuleUnitCount(villageData, unitsDesc, key)
        }

        if (maxUnits[key] === undefined) {
            unitsDistr[key] = 0
        }
        else {
            console.log("getting min of " + unitsDistr[key] + " and " + maxUnits[key])
            unitsDistr[key] = Math.min(unitsDistr[key], maxUnits[key])
            console.log("result: " + unitsDistr[key])
        }
    })

    return unitsDistr
}


function calculateRuleUnitCount(village, rule, unitName) {
    let count = 0
    let strategy = rule["strategy"]

    if (strategy["type"] === "interpolate") {
        let xVal = village[strategy["key"]]
        let values = strategy["values"]
        let referenceVal = 0
        let prevX = 0
        let prevY = 0
        let nextX = 0
        let nextY = 0

        if (strategy["reference"] === "cGroup") {
            referenceVal = village["recommendedUnits"][unitName]
        }
        else {
            referenceVal = village[strategy["reference"]]
        }

        console.log("reference value: " + referenceVal)
        /* 
            other references here
            ...
        */

        let prevIter = 0
        let valueSet = false
        Object.keys(values).forEach(v => {
            console.log("values[v]: " + values[v])
            console.log("xVal: " + xVal)
            if (!valueSet && v > xVal) {
                console.log("found range for " + xVal + ": " + prevIter + " - " + v)
                valueSet = true
                let py = values[prevIter] 
                let ny = values[v]

                prevX = prevIter
                if (py[py.length - 1] === "%") {
                    prevY = referenceVal * parseFloat(py.substring(0, py.length - 1)) / 100.0
                }
                else {
                    prevY = parseInt(py)
                }

                nextX = v
                if (ny[ny.length - 1] === "%") {
                    nextY = referenceVal * parseFloat(ny.substring(0, ny.length - 1)) / 100.0
                }
                else {
                    nextY = parseInt(ny)
                }
            }
            prevIter = v
        })

        if (!valueSet) {
            nextX = prevIter
            if (values[prevIter][values[prevIter].length - 1] === "%") {
                nextY = referenceVal * parseFloat(values[prevIter].substring(0, values[prevIter].length - 1)) / 100.0
            }
            else {
                nextY = parseInt(values[prevIter])
            }
        }

        console.log("interpolating...")
        console.log("x val: " + xVal)
        console.log("prev x: " + prevX)
        console.log("prev y: " + prevY)
        console.log("next x: " + nextX)
        console.log("next y: " + nextY)

        if (strategy["order"] === "linear") {
            if (xVal < prevX)
                count = prevY
            else if (xVal > nextX)
                count = nextY
            else
                count = prevY + (xVal - prevX)/(nextX - prevX)*(nextY - prevY)
        }
        /*
            other interpolation types here
            ...
        */
    }
    if (strategy["type"] === "constant") {
        count = parseInt(strategy["value"])
    }
    /*
        other strategies here
        ...
    */

    if (rule["min"] !== undefined) {
        count = Math.max(rule["min"], count)
    }
    if (rule["max"] !== undefined) {
        count = Math.min(rule["max"], count)
    }

    return parseInt(count)
}


function handleCustomAttack(villageData, availableUnits, unitsDesc) {
    let units = getCAttackUnits(villageData, availableUnits, unitsDesc)
    let unitsSum = getUnitsSum(units)
    let attackButton = undefined

    if (unitsSum == 0) {
        return false
    }

    if (compareDicts(units, currentATemplate)) {
        attackButton = villageData["sendA"]
    }
    else if (compareDicts(units, currentBTemplate)) {
        attackButton = villageData["sendB"]
    }
    else if (compareDicts(units, villageData["recommendedUnits"])) {
        attackButton = villageData["sendC"]
    }
    else if(nextTemplates.length == 0 || (nextTemplates.length == 1 && !compareDicts(nextTemplates[0], units))) {
        nextTemplates.push(units)
    }

    if (attackButton !== undefined && unitsNotSent + unitsSum <= getUnitsSum(availableUnits)) {
        console.log("attacking village")
        clickButton(attackButton)
        attacksSent += 1
        return true
    }

    unitsNotSent += unitsSum

    return false
}


function getAvailableUnits() {
    let units = {}
    let homeUnitsTable = document.getElementById("units_home").firstElementChild.childNodes
    let checkedRow = homeUnitsTable[0].getElementsByTagName("th")
    let quantitiesRow = homeUnitsTable[1].getElementsByTagName("td")

    for (let i = 0; i < TEMPLATE_UNITS.length; i++) {
        let isUnitUsed = checkedRow[i+1].getElementsByTagName("input").length > 0
        isUnitUsed = !isUnitUsed || checkedRow[i+1].getElementsByTagName("input")[0].checked

        if (isUnitUsed) {
            units[TEMPLATE_UNITS[i]] = parseInt(quantitiesRow[i+1].innerHTML)
        }
    }

    return units
}


function getTemplatesDataRow(rowI) {
    return document.getElementsByClassName("vis")[2].getElementsByTagName("tbody")[0].getElementsByTagName("tr")[rowI].getElementsByTagName("td")
}


/* reloads the page */
function changeTemplates() {
    console.log("changing templates")
    let availableUnits = getAvailableUnits()
    let dataRows = [getTemplatesDataRow(1), getTemplatesDataRow(3)]
    let changeButton = document.getElementsByClassName("vis")[2].getElementsByTagName("tbody")[0].getElementsByTagName("tr")[0].lastElementChild.firstElementChild.firstElementChild

    for (let i = 0; i < TEMPLATE_UNITS.length; i++) {
        for (let j = 0; j < nextTemplates.length; j++) {
            let unitsCount = nextTemplates[j][TEMPLATE_UNITS[i]]
            if (unitsCount === undefined) {
                unitsCount = 0
            }

            unitsCount = Math.min(unitsCount, availableUnits[TEMPLATE_UNITS[i]])
            if (!isNaN(unitsCount)) {
                enterInputValue(dataRows[j][i].firstElementChild, unitsCount)
            }
        }
    }

    clickButton(changeButton)
}


function loadCurrentTemplates() {
    console.log("loading templates")
    currentATemplate = {}
    currentBTemplate = {}

    let dataARow = getTemplatesDataRow(1)
    let dataBRow = getTemplatesDataRow(3)

    for (let i = 0; i < TEMPLATE_UNITS.length; i++) {
        currentATemplate[TEMPLATE_UNITS[i]] = parseInt(dataARow[i].firstElementChild.value)
        currentBTemplate[TEMPLATE_UNITS[i]] = parseInt(dataBRow[i].firstElementChild.value)
    }

    console.log("current A template: " + JSON.stringify(currentATemplate))
    console.log("current B template: " + JSON.stringify(currentBTemplate))
}


function getUnitsSum(units) {
    sum = 0
    for (let i = 0; i < Object.keys(units).length; i++) {
        let key = Object.keys(units)[i]
        if (key !== "spy") {
            sum += parseInt(units[key])
        }
    }

    return sum
}


function getDictSum(d) {
    sum = 0
    for (let i = 0; i < Object.keys(d).length; i++) {
        let key = Object.keys(d)[i]
        sum += parseInt(d[key])
    }

    return sum
}


function compareDicts(d1, d2) {
    for (let i in Object.keys(d1)) {
        let key = Object.keys(d1)[i]
        if (d1[key] != d2[key] && !(d1[key] == 0 && d2[key] === undefined)) {
            return false
        }
    }

    for (let i in Object.keys(d2)) {
        let key = Object.keys(d2)[i]
        if (d2[key] != d1[key] && !(d2[key] == 0 && d1[key] === undefined)) {
            return false
        }
    }

    return true
}


function loadVillagesData() {
    console.log("loading data from page")
    villagesData = []

    var plunderList = document.getElementById("plunder_list")
    var tBody = plunderList.getElementsByTagName("tbody")[0]
    var tableRows = tBody.getElementsByTagName("tr")

    for (let i = 2; i < tableRows.length; i++) {
        let row = tableRows[i].getElementsByTagName("td")
        let rowData = {
            "cAvailable": row[5].firstElementChild.innerHTML !== "?",
            /*prevSuccessState*/
            // "fullLoot": /0.png/.test(row[2].getAttribute("src")),
            // "coords": /\d+\|\d+/.exec(row[3].innerText)[0],
            "secondsAgo": dateTillNowToSeconds(row[4].innerText),
            "wall": parseInt(row[6].innerText),
            "isAttacked": row[3].getElementsByTagName("img").length > 0,
            "distance": parseFloat(row[7].innerText),
            "sendA": row[8].firstElementChild,
            "sendB": row[9].firstElementChild
        }

        let resources = row[5].childNodes

        if (resources.length == 7) {
            rowData["resources"] = parseInt(resources[1].childNodes[1].innerText)
                                 + parseInt(resources[3].childNodes[1].innerText)
                                 + parseInt(resources[5].childNodes[1].innerText)
        }
        if (rowData["resources"] !== undefined) {
            rowData["income"] = rowData["resources"]/(rowData["secondsAgo"] + rowData["distance"]*600)
        }
        if (rowData["cAvailable"]) {
            rowData["sendC"] = row[10].firstElementChild
            /*possibleLoot*/

            /* two consecutive numbers in onclick string */
            let sendCPostCommand = rowData["sendC"].getAttribute("onclick").toString()

            if (sendCPostCommand === "return false") {
                continue
            }

            let regex = /\d+/g
            rowData["villageID"] = regex.exec(sendCPostCommand)[0]
            rowData["reportID"] = regex.exec(sendCPostCommand)[0] 

            rowData["recommendedUnits"] = JSON.parse(rowData["sendC"].getAttribute("data-units-forecast"))
        }

        villagesData.push(rowData)
    }
    console.log("data loaded")
}
